from datetime import timedelta, datetime
from .models import db, User, AuthToken, History
from secrets import token_hex as token_gen


def create_user(username, password):
    """Create a new user with the given username and password."""

    usr = User(username=username, password=User.hash_pw(password))
    db.session.add(usr)
    try:
        db.session.commit()
        return User.query.filter_by(username=username).first()
    except Exception as e:
        print(f"Exception occurred: {e}")
        return None


def add_history(token, url, input, output):
    """Save a record to the history table for what a user did."""

    user = get_user_from_token(token)
    function = url.split("/")[-1]
    time = datetime.now()

    new_record = History(
        user=user.id,
        function=function,
        time=time,
        input=input[:256],
        output=output[:256],
    )
    db.session.add(new_record)
    db.session.commit()


def authenticate_user(username, password):
    """Find a user with the correct username and password. Tokens expire 24 hours after
    they are issued.."""

    usr = User.query.filter_by(username=username).first()
    if usr and usr.check_pw(password):
        tok = (
            AuthToken.query.filter_by(user=usr.id)
            .filter(AuthToken.expiry >= datetime.now())
            .first()
        )

        if not tok:
            new_token_val = token_gen(16)
            while token_exists(new_token_val):  # pragma: no cover
                new_token_val = token_gen(16)

            tok = AuthToken(
                user=usr.id,
                token=new_token_val,
                expiry=(datetime.now() + timedelta(hours=24)),
            )
            db.session.add(tok)
            db.session.commit()

        return tok.token
    else:
        return None


def user_exists(username):
    """Return whether or not a user exists."""

    return User.query.filter_by(username=username).first() is not None


def token_exists(token):
    """Return whether or not a token exists."""

    return AuthToken.query.filter_by(token=token).first() is not None


def get_user_from_token(token):
    """Return the user instance from a token."""

    return User.query.filter_by(
        id=AuthToken.query.filter_by(token=token).first().user
    ).first()


def get_serialized_history(user):
    """Get a user's history from the database and serialize it so that it can be sent
    back to them."""

    records = History.query.filter_by(user=user.id).all()
    serialized_records = [
        {
            "user": user.username,
            "function": r.function,
            "time": r.time,
            "input": r.input,
            "output": r.output,
        }
        for r in records
    ]
    return serialized_records
